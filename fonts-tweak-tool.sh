#! /bin/sh
top_srcdir=$(dirname $0)
top_builddir=$(dirname $0)/build
export GI_TYPELIB_PATH=${top_builddir}/fontstweak/.libs:${top_builddir}/fontstweak:${top_srcdir}/fontstweak/.libs:${top_srcdir}/fontstweak:$GI_TYPELIB_PATH
export LD_LIBRARY_PATH=${top_builddir}/fontstweak/.libs:${top_builddir}/fontstweak:${top_srcdir}/fontstweak/.libs:${top_srcdir}/fontstweak:$LD_LIBRARY_PATH

${DEBUG} ${top_srcdir}/fonts-tweak-tool $@
