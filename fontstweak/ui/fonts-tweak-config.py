# -*- coding: utf-8 -*-
# fonts-tweak-config.py
# Copyright (C) 2012-2015 Red Hat, Inc.
#
# Authors:
#   Akira TAGOH  <tagoh@redhat.com>
#
# This library is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import collections
import gi
import os
import sys
from gi.repository import Gtk
from gi.repository import Pango
from xdg import BaseDirectory
try:
    from tweaks import FontsTweak
except ImportError:
    from fontstweak.tweaks import FontsTweak
try:
    from util import FontsTweakUtil
except ImportError:
    from fontstweak.util import FontsTweakUtil

def N_(str): return str

class FontsTweakConfigItem(Gtk.ListBoxRow):

    def __init__(self, usrfile=None, sysfile=None, data=None):
        if usrfile == None and sysfile == None:
            raise exception("No valid filename")

        Gtk.ListBoxRow.__init__(self)
        self.enabled_by_system = False
        self.usrfile = usrfile
        self.fttsysfile = sysfile
        self.sysfile = sysfile
        self.box = Gtk.HBox()
        self.box.props.border_width = 10
        self.box.props.spacing = 4
        self.box.get_style_context().add_class('config-item')
        self.label = Gtk.Label.new("")
        self.label.props.ellipsize = Pango.EllipsizeMode.END
        self.label.props.xalign = 0.0
        self.note = Gtk.Label.new("")
        self.note.props.ellipsize = Pango.EllipsizeMode.END
        self.note.props.xalign = 0.0
        self.sw = Gtk.Switch()
        self.sw.props.vexpand = False
        self.sw.props.valign = Gtk.Align.CENTER
        self.box.pack_start(self.sw, False, False, 0)
        self.box.pack_start(self.label, True, True, 10)
        self.box.pack_start(self.note, True, True, 10)
        self.update()
        self.label.set_label(self.get_shortname())
        self.sw.connect('notify::active', self.on_switch_activated, data)

        self.add(self.box)

    def get_name(self):
        return self.sysfile or self.usrfile

    def get_shortname(self):
        return os.path.basename(self.get_name())

    def get_sys_filename(self):
        return self.sysfile

    def update(self):
        if not self.enabled_by_system:
            if self.usrfile != None:
                self.sysfile = self.analyze(self.usrfile)
            self.sw.set_sensitive(self.sysfile != None)
            self.sw.set_active(self.usrfile != None)
            if self.sysfile == None:
                self.note.set_label('[' + _('user-defined file') + ']')

    def analyze(self, path):
        if os.path.islink(path):
            return os.path.realpath(path)
        return None

    def set_enabled_by_system(self, flag):
        self.enabled_by_system = flag
        if self.enabled_by_system:
            self.note.set_label('[' + _('enabled by system') + ']')
            self.sw.set_sensitive(False)
            self.sw.set_active(True)

    def on_switch_activated(self, sw, gparams, data):
        if not self.enabled_by_system:
            if sw.get_active():
                self.usrfile = os.path.join(BaseDirectory.load_first_config('fontconfig'), 'conf.d', os.path.basename(self.sysfile))
                os.symlink(self.sysfile, self.usrfile)
            else:
                os.unlink(self.usrfile)
                self.usrfile = None
        self.update()

class FontsTweakConfigUI(Gtk.Box, FontsTweak):

    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        FontsTweak.__init__(self, 'config', N_('Fontconfig recipes'))

        self.config = None
        builder = FontsTweakUtil.create_builder('fonts-tweak-config.ui')
        builder.connect_signals(self)
        w = builder.get_object('fonts-config-ui')
        self.pack_start(w, True, True, 0)
        self.show_all()

        self.scrolled_window = builder.get_object('scrolledwindow-config-list')
        self.sw_enabled_by_system = builder.get_object('switch-enabled-by-system')
        self.sizegroup = Gtk.SizeGroup.new(Gtk.SizeGroupMode.HORIZONTAL)

        if BaseDirectory.load_first_config('fontconfig') == None:
             BaseDirectory.save_config_path('fontconfig')
        d = os.path.join(BaseDirectory.load_first_config('fontconfig'), 'conf.d')
        try:
            filenames = os.listdir(d)
        except OSError:
            filenames = []

        self.listbox = Gtk.ListBox()
        self.listbox.get_style_context().add_class('config-list')
        self.scrolled_window.add(self.listbox)
        self.scrolled_window.show_all()
        self.items = {}
        for f in filenames:
            i = FontsTweakConfigItem(os.path.join(d, f))
            if i.get_name() in self.items:
                print("duplicate symlinks for %s" % i.get_name())
                continue
            self.items[i.get_name()] = i

        d = os.path.join('/', 'usr', 'share', 'fontconfig', 'conf.avail')
        try:
            filenames = os.listdir(d)
        except OSError:
            filenames = []

        for f in filenames:
            # self somehow needs to be set as the user data
            # to be connected to the gtk signal. otherwise
            # AttributeError is raised at os_switch_activated
            # and no attributes defined in certain objects.
            # even though it is initialized at __init__.
            # really weird.
            i = FontsTweakConfigItem(None, os.path.join(d, f), self)
            if not i.get_sys_filename() in self.items:
                self.items[i.get_sys_filename()] = i

        d = os.path.join('/', 'etc', 'fonts', 'conf.d')
        filenames = os.listdir(d)
        sysconf = {}
        for f in filenames:
            sysconf[os.path.realpath(os.path.join(d, f))] = os.path.join(d, f)

        d = collections.OrderedDict(sorted(self.items.items()))
        for (k, v) in d.items():
            self.sizegroup.add_widget(v.label)
            v.show_all()
            self.listbox.add(v)
            if k in sysconf:
                v.set_enabled_by_system(True)
                if not self.sw_enabled_by_system.get_active():
                    v.set_visible(False)

    def is_enabled(self):
        return True

    def on_switch_enabled_by_system_active(self, sw, gparams):
        for (k, v) in self.items.items():
            if v.enabled_by_system:
                if not self.sw_enabled_by_system.get_active():
                    v.set_visible(False)
                else:
                    v.set_visible(True)

    def do_set_config(self):
        pass

TWEAKS_UI = [FontsTweakConfigUI()]
